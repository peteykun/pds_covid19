#/usr/bin/python3
import git, csv

# update git repository
git.cmd.Git('covid-19').pull()

countries = ['India', 'Belgium', 'Italy', 'US']
figures = dict()

for country in countries:
    figures[country] = []

with open('covid-19/data/countries-aggregated.csv') as csvfile:
    reader = csv.reader(csvfile)

    for row in reader:
        date, country, confirmed, recovered, deaths = row
        
        # skip header row
        if date == 'Date':
            continue

        confirmed = int(confirmed)
        recovered = int(recovered)
        deaths = int(deaths)

        if country in countries:
            if confirmed + recovered + deaths > 0:
                figures[country] += [(date, confirmed, recovered, deaths)]

for country in countries:
    with open('data/%s.csv' % country, 'w+') as f:
        f.write('Date\tConfirmed\tRecovered\tDeaths\n')

        for item in figures[country]:
            f.write('%s\t%d\t%d\t%d\n' % item)
